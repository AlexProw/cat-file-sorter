# -*- coding: utf-8 *-*
from pydoc import classname
import tkinter as tk  # importar un módulo que no pertenece a un paquete
from tkinter import filedialog
from mover import CatFileMover    
class InitialScreen():
    def __init__(self):
        self.cat_mover = tk.Tk()
        self.cat_mover.title("Cat file mover")
        self.cat_mover.geometry("600x480")

        self.source_route = tk.StringVar()
        self.destiny_route = tk.StringVar()

        self.source_label = tk.Label(self.cat_mover, text="Source Directory").grid(row=0)
        self.source_field = tk.Entry(self.source_label, textvariable=self.source_route).grid(row=0,column=1)

        self.destiny_label = tk.Label(self.cat_mover, text="Destination Directory").grid(row=1)
        self.file_field2 = tk.Entry(self.destiny_label, textvariable=self.destiny_route).grid(row=1,column=1)

        self.source_select = tk.Button(self.cat_mover, text="open source", command=lambda: self.selectFolder(self.source_route)).grid(row=0,column=2)
        self.destiny_select = tk.Button(self.cat_mover, text="open destiny", command=lambda: self.selectFolder(self.destiny_route)).grid(row=1,column=2)
        self.start_button = tk.Button(self.cat_mover, text="Start", command=lambda: self.something()).grid(row=2,column=1)

        self.cat_mover.mainloop()


    def selectFolder(self, file_field):
        self.cat_mover.folder_box = tk.filedialog.askdirectory(initialdir = "/",title = "Select folder")
        print(self.cat_mover.folder_box)
        file_field.set(self.cat_mover.folder_box)

    def something(self):
        print("I will send the coso to the correct way: ")
        CatFileMover(self.source_route.get(),self.destiny_route.get())


InitialScreen()